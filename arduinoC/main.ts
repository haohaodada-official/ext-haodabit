enum linechoose {
    //% block=R1
    0x19,
    //% block=R2
    0x16,
    //% block=L1
    0x14,
    //% block=L2
    0x13
}

enum linechooseAD {
    //% block=R1
    0x07,
    //% block=R2
    0x09,
    //% block=L1
    0x05,
    //% block=L2
    0x03
}

enum linechoosedb {
    //% block=R1
    0x0f,
    //% block=R2
    0x11,
    //% block=L1
    0x0d,
    //% block=L2
    0x0b
}

//% color="#17959d" iconWidth=50 iconHeight=40
namespace haodabit {
    //% block="Motor[INDEX] run Direction[DIR] Speed[SD]" blockType="command"
    //% INDEX.shadow="dropdown" INDEX.options="INDEX" 
    //% DIR.shadow="dropdown" DIR.options="DIR" 
    //% SD.shadow="range" SD.params.min=0 SD.params.max=255 SD.defl=200
    export function motorRun(parameter: any, block: any) {
        let index = parameter.INDEX.code;
        let dir = parameter.DIR.code;
        let sd = parameter.SD.code;
        Generator.addInclude("addIncludeMicrobit_Motor", "#include <Microbit_Motor.h>");
        Generator.addObject("addMicrobit_Motor", "Microbit_Motor", `motorbit;`);
        Generator.addCode(`motorbit.motorRun(${index}, ${dir}, ${sd});`)
    }

    //% block="Motor[INDEX] run Direction[DIR] Speed[SD]" blockType="command"
    //% INDEX.shadow="dropdown" INDEX.options="INDEX"
    //% DIR.shadow="dropdown" DIR.options="DIR" 
    //% SD.shadow="range" SD.params.min=0 SD.params.max=255 SD.defl=200
    export function motorStop(parameter: any, block: any) {
        let index = parameter.INDEX.code;
        Generator.addInclude("addIncludeMicrobit_Motor", "#include <Microbit_Motor.h>");
        Generator.addObject("addMicrobit_Motor", "Microbit_Motor", `motorbit;`);
        Generator.addCode(`motorbit.motorStop(${index});`)
    }

    //% block="Ultrasonic port[PIN]" blockType="reporter"
    //% PIN.shadow="dropdown" PIN.options="PIN" 
    export function Ultrasonic(parameter: any, block: any) {
        let pin = parameter.PIN.code;
        Generator.addInclude("addIncludeHD_Ultrasonic_digit", "#include <HD_Ultrasonic_digit.h>");
        Generator.addObject(`addUltrasonic_digit_${pin}`, "Ultrasonic_digit", `myUltrasonic_${pin}(${pin});`);
        Generator.addCode(`(int)myUltrasonic_${pin}.distance_digit(400)`)
    }

    //% block="Tracer calibrate" blockType="command"
    export function Linecalibrate(parameter: any, block: any) {
        Generator.addInclude("addIncludeHD_Linefinder", "#include <HD_Linefinder.h>");
        Generator.addObject(`addHD_Linefinder`, "HD_Linefinder", `HD_Linefinder;`);
        Generator.addCode(`HD_Linefinder.calibrate();`)
    }

    //% block="read Tracer[AD] touch black" blockType="reporter"
    //% AD.shadow="dropdown" AD.options="linechooseAD" AD.defl="linechooseAD.R1"
    export function readlinead(parameter: any, block: any) {
        let ad = parameter.AD.code;
        Generator.addInclude("addIncludeHD_Linefinder", "#include <HD_Linefinder.h>");
        Generator.addObject(`addHD_Linefinder`, "HD_Linefinder", `HD_Linefinder;`);
        Generator.addCode(`HD_Linefinder.readline(${ad})`)
    }

    //% block="set Tracer[DB] [HEIGHT]" blockType="command"
    //% DB.shadow="dropdown" DB.options="linechoosedb" DB.defl="linechoosedb.R1"
    //% HEIGHT.shadow="number" HEIGHT.defl=0
    export function Lineheight(parameter: any, block: any) {
        let db = parameter.DB.code;
        let height = parameter.HEIGHT.code;
        Generator.addInclude("addIncludeHD_Linefinder", "#include <HD_Linefinder.h>");
        Generator.addObject(`addHD_Linefinder`, "HD_Linefinder", `HD_Linefinder;`);
        Generator.addCode(`HD_Linefinder.lineheight(${db}, ${height});`)
    }

    //% block="Tracer[LI] touch black or not" blockType="reporter"
    //% LI.shadow="dropdown" LI.options="linechoose" AD.defl="linechoose.R1"
    export function readline(parameter: any, block: any) {
        let li = parameter.LI.code;
        Generator.addInclude("addIncludeHD_Linefinder", "#include <HD_Linefinder.h>");
        Generator.addObject(`addHD_Linefinder`, "HD_Linefinder", `HD_Linefinder;`);
        Generator.addCode(`HD_Linefinder.readline(${li})`)
    }
}