#ifndef _HD_Linefinder_H_
#define _HD_Linefinder_H_
#include "Wire.h"
#include <Arduino.h>

class HD_Linefinder
{
public:
  HD_Linefinder();
  void write(uint8_t addr, uint8_t reg, uint8_t buf);
  uint8_t read(uint8_t addr, uint8_t reg);
  void calibrate();
  void lineheight(uint8_t reg, uint8_t buf);
  uint8_t readline(uint8_t reg);
};

#endif