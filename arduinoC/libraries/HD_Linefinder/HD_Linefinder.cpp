#include <HD_Linefinder.h>

HD_Linefinder::HD_Linefinder(){
  Wire.begin();
}

void HD_Linefinder::write(uint8_t addr, uint8_t reg, uint8_t buf){
  Wire.beginTransmission(addr); // transmit to device #8
  Wire.write(reg);              // sends one byte
  Wire.write(buf);
  Wire.endTransmission();    // stop transmitting
}

uint8_t HD_Linefinder::read(uint8_t addr, uint8_t reg){
  Wire.beginTransmission(addr); //Start transmission to device 
  Wire.write(reg); //Sends register address to read rom
  Wire.endTransmission(); //End transmission
  
  Wire.requestFrom(addr, 1);//Send data n-bytes read
  return Wire.read();
}

void HD_Linefinder::calibrate(){
  write(0x52, 0x21, 0x61);
  write(0x52, 0x22, 0x7a);
  write(0x52, 0x23, 0x68);
  write(0x52, 0x24, 0x65);
}

void HD_Linefinder::lineheight(uint8_t reg, uint8_t buf){
  write(0x52, reg, buf);
}

uint8_t HD_Linefinder::readline(uint8_t reg){
  read(0x52, reg);
}
